import './App.css';
import React, {useState, useEffect} from 'react';
import SingleCard from './Components/SingleCard';


//Array
const CardImg = [
  {src: '/img/FotoJet (2).jpg',matched : false},
  {src: '/img/200d.jpg'       ,matched : false},
  {src: '/img/m200d.jpg'      ,matched : false},
  {src: '/img/j200d.jpg'      ,matched : false},
  {src: '/img/p200d.jpg'      ,matched : false},
  {src: '/img/Fotojet (3).jpg',matched : false},
];

function App () {
  const [card, setCard] = useState ([]);
  const [turn, setTurn] = useState (0);
  const [selecteOne, setSelecteOne] = useState (null);
  const [selecteTwo, setSelecteTwo] = useState (null);
  const [disable , setDisable]= useState(false);

  //suffle card
  const SuffleCard = () => {
    const Suffle = [...CardImg, ...CardImg]
      .sort (() => Math.random () - 0.5)
      .map (card => ({...card, id: Math.random ()}));

      
    setCard (Suffle);
    setTurn (0);
    //console.log (Suffle);
    setSelecteOne(null)
      setSelecteTwo(null)
  };

  //handlechoice
  const handleChoice = card => {
    selecteOne ? setSelecteTwo (card) : setSelecteOne (card);
  };

  //compare the card
  useEffect (
    () => {
     
      if (selecteOne && selecteTwo) {
        setDisable(true)
        if (selecteOne.src === selecteTwo.src) {
         setCard(prevcard => {
          return prevcard.map((card)=>{
            if(card.src === selecteOne.src){
                 return{...card , matched: true}
            }else{
              return card
            }
          })
         })
          reset ();
        } else {
         
          setTimeout(()=> reset() , 1000)
        }
      }
    },
    [selecteOne, selecteTwo]
  );
  //console.log(card);

  // for automatic start 
  useEffect(()=>{
SuffleCard()
  },[])

  //reset your turn
  const reset = () => {
    setSelecteOne (null);
    setSelecteTwo (null);
    setTurn (prevTurn => prevTurn + 1);
    setDisable(false)
  };

  return (
    <div className="App">
      <h1>Card Game</h1>
      <button onClick={SuffleCard}>New Game</button>
      <div className="card-grid">
        {card.map (card => (
          <SingleCard key={card.id} card={card} handleChoice={handleChoice} 
          flipped={card === selecteOne|| card === selecteTwo || card.matched }
            disable={disable}
          />
        ))}
      </div>
      <p>Move : {turn}</p>
    </div>
  );
}

export default App;
